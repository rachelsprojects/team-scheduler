<?

$days = [ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ];
$teams = [ "alexa", "anisha", "rachel", "rebekah", "rose", "shawnee" ];
$status = "";

function TimeToAmPm( $time )
{
    $ampm = "am";
    if ( $time > 12 )
    {
        $time = $time % 12;
        $ampm = "pm";
    }

    return $time . " " . $ampm;
}

function LoadSchedule()
{
    $listPath = "schedule.json";
    $listJson = file_get_contents( $listPath );
    $listArray = json_decode( $listJson, true );

    return $listArray;
}

function UpdateSchedule()
{
    global $days;
    global $schedule;
    global $status;
    
    $teammate = $_POST[ "who" ];
    $schedule[$teammate] = array();

    foreach ( $days as $dk=>$day )
    {
        for ( $time = 0; $time < 24; $time++ )
        {            
            $str = $day . "-" . $time;
            if ( in_array( $str, $_POST["available"] ) )
            {
                array_push( $schedule[$teammate], $str );
            }
        }
    }

    $status .= "<br> Updated schedule for $teammate";
}

function SaveSchedule()
{
    $listPath = "schedule.json";
    global $schedule;
    global $status;
    
    $listJson = json_encode( $schedule, JSON_PRETTY_PRINT );

    if ( file_put_contents( $listPath, $listJson ) ) {
        $status .= "<br> Saved schedule to file";

        // Reload
        $listJson = file_get_contents( $listPath );
        $listArray = json_decode( $listJson, true );
    } else {
        $status .= "<br> Error saving list";
    }
}

$schedule = LoadSchedule();

if ( isset( $_POST[ "who" ] ) ) {
    UpdateSchedule();
    SaveSchedule();
}

echo( "<div class='container'><div class='row'><div class='col-xs-12'>$status</div></div></div>" );

?>
