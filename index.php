<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title> What's your availability? </title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="jquery/jquery-3.3.1.min.js"></script>

    <style type="text/css">
        h2 { font-size: 14pt; }
        h1 { font-size: 18pt; }
        .status { text-align: center; background: #ffffaa; }
        #update-me { display: none; }
        .available-times { font-size: 10pt; }
        ul.team { list-style-type: none; padding: 0; margin: 0; }
        ul.team li { padding: 0; margin: 0; }

        .alexa { color: #a60000; }
        .anisha { color: #ff5d00; }
        .rachel { color: #908b00; }
        .rebekah { color: #069000; }
        .rose { color: #003690; }
        .shawnee { color: #570090; }

        .availability { font-weight: bold; }

        input[type=radio],
        input[type=checkbox] { border: solid 1px #000000; }

        .select-days select { width: 100%; }
        .schedule-list .row { border-bottom: solid 1px #aaa; }
        .schedule-list p { padding: 0; margin: 0; }
        .schedule-list h2 { font-size: 12pt; }
        .schedule-list h3 { font-size: 10pt; margin: 0; padding: 0; }
        .schedule-list { font-size: 8pt; }
        .schedule-list .hour-block { border: solid 1px #aaaaaa; background: #ffffff; margin: 5px 0; text-align: center; }
    </style>

    <script>
        $( document ).ready( function() {
            $( "#open-updater" ).click( function() {
                if ( $( "#update-me" ).css( "display" ) == "none" )
                {
                    // Show
                    $( "#update-me" ).fadeIn( "fast" );
                    $( "#open-updater" ).html( "Hide times" );
                }
                else
                {
                    // Hide
                    $( "#update-me" ).fadeOut( "fast" );
                    $( "#open-updater" ).html( "Update my availability!" );
                }
            } );

            $( "#select-all" ).click( function() {
                var days = [ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ];

                for ( var d = 0; d < days.length; d++ )
                {
                    for ( var h = 0; h < 24; h++ )
                    {
                        var name = days[d] + "-" + h;
                        $( "#" + name ).attr( "selected", "selected" );
                    }
                }
            } );

            $( "#clear-all" ).click( function() {
                var days = [ "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" ];

                for ( var d = 0; d < days.length; d++ )
                {
                    for ( var h = 0; h < 24; h++ )
                    {
                        var name = days[d] + "-" + h;
                        $( "#" + name ).attr( "selected", false );
                    }
                }
            } );
        } );
    </script>
  </head>
  <body>

    <? include_once( "process.php" ); ?>

    <div class="container">
        <h1>What's your availability?</h1>

        <br><br>

        <button type="" id="open-updater" class="btn btn-primary form-control">Update my availability!</button>
        <br><br>
        <div class="new-item row" id="update-me">
            <div class="col-xs-12">
                <form method="post">
                    <p>Select which times you are able to come to a Moosader meeting.</p>
                    <p>Hold down ctrl while clicking to select multiple from a list.</p>

                    <h2>1. Who are you?</h2>
                    <div class="row">
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="alexa" id="team-alexa">
                            <label for="team-alexa" class="alexa">Alexa</label>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="anisha" id="team-anisha">
                            <label for="team-anisha" class="anisha">Anisha</label>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="rachel" id="team-rachel">
                            <label for="team-rachel" class="rachel">Rachel</label>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="rebekah" id="team-rebekah">
                            <label for="team-rebekah" class="rebekah">Rebekah</label>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="rose" id="team-rose">
                            <label for="team-rose" class="rose">Rose</label>
                        </div>
                        <div class="col-md-2 col-xs-6">
                            <input type="radio" class="form-check-input" name="who" value="shawnee" id="team-shawnee">
                            <label for="team-shawnee" class="shawnee">Shawnee</label>
                        </div>
                    </div>

                    <br><br>
                    <h2>2. Select times you would want to meet</h2>
                    <p>
                        <button type="submit" name="new" class="btn btn-success form-control">Save changes</button>
                    </p>

                    <div class="row select-days">
                        <div class="col-xs-12 col-md-2">
                            <p>Saturday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="saturday-<?=$t?>" id="saturday-<?=$t?>" value="saturday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                            
                            <p>Sunday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="sunday-<?=$t?>" id="sunday-<?=$t?>" value="sunday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <p>Monday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="monday-<?=$t?>" id="monday-<?=$t?>" value="monday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <p>Tuesday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="tuesday-<?=$t?>" id="tuesday-<?=$t?>" value="tuesday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <p>Wednesday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="wednesday-<?=$t?>" id="wednesday-<?=$t?>" value="wednesday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <p>Thursday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="thursday-<?=$t?>" id="thursday-<?=$t?>" value="thursday-<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-md-2">
                            <p>Friday</p>
                            <select multiple class="form-control" name="available[]">
                                <? for ( $t = 9; $t < 24; $t++ ) { ?>
                                    <option name="friday-<?=$t?>" id="friday-<?=$t?>" value="y--<?=$t?>"><?=TimeToAmPm( $t )?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>

                    <br><br>
                    <input type="button" class="button btn btn-primary" id="select-all" value="Select all times">
                    <input type="button" class="button btn btn-warning" id="clear-all" value="Clear all times">
                    <br><br>
                    
                    <button type="submit" name="new" class="btn btn-success form-control">Save changes</button>
                </form>
            </div>
        </div>

        <br><br>

        <hr>

        <div class="schedule-list">
            <div class="row" id="schedule">
                <? foreach ( $days as $day ) { /* Day */ ?>
                    <div class="col-xs-6 col-sm-3 col-md-1"><h2><?=$day?></h2>
                    <? for ( $t = 9; $t < 24; $t++ ) { /* Hour */ ?>
                        <div class="hour-block">
                            <h3><?=TimeToAmPm( $t )?></h3>
                            <ul class="team">
                                <? foreach( $teams as $tk=>$team ) { /* Team */ ?>
                                    <?
                                    $str = $day . "-" . $t;                    
                                    if ( in_array( $str, $schedule[$team] ) )
                                    {
                                        echo( "<li class='" . $team . " availability'>" . $team . "</li>" );
                                    }
                                    ?>
                                <? } /* Team */ ?>
                            </ul>
                        </div>
                    <? } /* Hour */ ?>
                    </div>
                <? } /* Day */ ?>
                <div class="col-md-4 col-sm-3 col-xs-6">
                    <h2>Common days</h2>

                    <ul>
                    <? for ( $total = 6; $total > 2; $total-- ) {?>
                        <?
                            // Count how many people can make a given day
                            foreach ( $days as $day ) {
                                for( $t = 9; $t < 24; $t++ ) {
                                    $totalMembers = 0;
                                    $memberString = "";
                                    foreach( $teams as $team ) {
                                        $str = $day . "-" . $t;  
                                        if ( in_array( $str, $schedule[$team] ) ) {
                                            $memberString .= "<li class='$team'>$team</li>";
                                            $totalMembers += 1;
                                        }
                                    } /* team */

                                    if ( $totalMembers == $total )
                                    {
                                        echo( "<li>$day " . TimeToAmPm( $t ) . " - $total Members" );
                                        echo( "<ol>" );
                                        echo( $memberString );
                                        echo( "</ol>" );
                                        echo( "</li>" );
                                    }
                                } /* hour */
                            } /* day */
                        ?>
                    <? } ?>
                    </ul>
                </div>
            </div>
            
        </div>
 
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
